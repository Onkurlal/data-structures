# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self,value,link=None):
        self.value = value
        self.link = link


class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.length = 0

    def traverse(self, index):
        if index >= self.length:
            raise IndexError("Index out of range")
        current_node = self.head
        i = 0
        while i < index:
            current_node = current_node.link
            i += 1
        return current_node

    def insert(self, value, index=None):
        new_node = LinkedListNode(value)


        if self.head is None:
            self.head = new_node
            self.tail = new_node
        elif index is not None and index < self.length:
            if index == 0:
                new_node.link = self.head
                self.head = new_node
            else:
                previous_node = self.traverse(index - 1)
                new_node.link = previous_node.link
                previous_node.link = new_node
        else:
            self.tail.link = new_node
            self.tail = new_node

        self.length += 1

    def get(self, index):
        if index < 0:
            index = self.length + index
        current_node = self.traverse(index)
        return current_node.value

    def remove(self, index):
        self.length -= 1
        if index == 0:
            current_node = self.head
            head = self.head.link
            return current_node.value

        previous_node = self.traverse(index - 1)
        current_node = previous_node.link
        previous_node.link = current_node.link

        if current_node.link is None:
            self.tail = previous_node
        return current_node.value
